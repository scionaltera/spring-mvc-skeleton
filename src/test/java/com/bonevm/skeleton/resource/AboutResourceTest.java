package com.bonevm.skeleton.resource;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Date;

import static org.testng.Assert.*;
import static org.mockito.Mockito.*;

public class AboutResourceTest {
    @Mock
    private Model model;

    @InjectMocks
    private AboutResource aboutResource = new AboutResource();

    @BeforeClass
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAbout() {
        ResponseEntity<String> entity = aboutResource.about();

        assertEquals(entity.getStatusCode().value(), HttpStatus.OK.value());
    }

    @Test
    public void testJsp() {
        String view = aboutResource.aboutJsp(model);

        verify(model).addAttribute(eq("time"), any(Date.class));

        assertEquals(view, "about");
    }
}
