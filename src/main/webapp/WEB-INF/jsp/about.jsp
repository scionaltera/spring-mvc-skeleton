<html>
<head>
    <title>About</title>
    <script type="text/javascript">
        var config = {
            contextPath: '${pageContext.request.contextPath}'
        };
    </script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/about.css">
</head>
<body>
<div id="container">
    <img id="topshelf" src="${pageContext.request.contextPath}/img/topshelf.jpg" alt="Congratulations!"/>
    <p>Hello, ${pageContext.request.remoteUser}! The current time is: ${time}</p>
</div>
<div>
    <form action="/logout" method="post">
        <input type="submit" value="Log Out"/>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
</div>
</body>
</html>