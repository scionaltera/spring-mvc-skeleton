package com.bonevm.skeleton.resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;

@Component
@RequestMapping("/")
public class AboutResource {
    @NotNull
    @RequestMapping(value = "/rest/v1/about", method = RequestMethod.GET)
    public ResponseEntity<String> about() {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @NotNull
    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String aboutJsp(Model model) {

        model.addAttribute("time", new Date());

        return "about";
    }
}
