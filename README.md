# Description
[![Build Status](https://drone.io/bitbucket.org/scionaltera/spring-mvc-skeleton/status.png)](https://drone.io/bitbucket.org/scionaltera/spring-mvc-skeleton/latest)

This is a bare bones skeleton project for creating a Spring MVC REST service quickly and easily. It includes a number of nice features for logging and testing. If you'd like to use it, simply fork the project and start creating your services.

I have attempted to keep everything as simple and standard as possible.

## Local Installation
The website follows the typical Maven structure and is designed to be easy to set up locally for testing. You will need the following tools installed to run the site locally:

1. Java 7 or later.
2. Maven 3.1.1 or later.

To start up the site, you just need to run the following command from the command line:

    mvn jetty:run-war

After running the command, Maven will download the appropriate version of the Jetty web server, all the dependencies, compile the code, run the tests, and finally start up the web server for you. You can visit the local version of the site at `http://localhost:8080` and check it out.

## Contributing
If you would like to contribute to the project, please feel free to submit a pull request. If you have any questions, just ask one of the committers and we'll be glad to help you out.

For the best chance of success getting your pull request approved, please try to do the following few things:

1. Match the coding style of existing code as best as possible.
2. Write unit tests against your new code.
3. Document your work, or include updates to existing documentation.
